

# 2º Semestre - Pineapple
<div align="center">
    <img src="./Pinneaple/pin.jpg" width="320" />
    <div height="50"></div>
</div>
Pineapple nasceu para atender as demandas de gerenciamento de atividades da empresa Necto.<br>
Era de interesse do cliente um sistema flexível, intuitivo e de fácil utilização, que possuísse um diagrama de Gantt para o auxílio no planejamento das atividades da empresa. Foi apresentado diversos projetos com ideia semelhante mas nenhum com todos os requisitos desejáveis pelo cliente.<br>
Pineapple foi desenvolvido para servir como a ferramenta ideal para as empresas que gostariam de ter uma praticidade maior para o planejamento de suas atividades e distribuição de recursos pela mesma. Com uma interface gráfica leve e intuitiva o usuário poderá gerenciar todos os entregáveis de sua empresa com uma maior agilidade.

## Objetivos
* Cadastro de Funcionários: 
O cadastro de funcionários foi implementado para cadastrar todos os participantes da empresa e atribuir a projeto e tarefas que seriam posteriormente atribuídos. Desta forma o nome do funcionário atribuído poderia ser visualizado no diagrama com suas devidas tarefas.
* Cadastro de Projetos:
Para realizar a inserção de projetos presentes na empresa, foi realizado o cadastro dos mesmos, onde nele seriam atribuídos os funcionários envolvidos.
* Cadastro de Tarefas:
Todo projeto possuía um determinado número de tarefas a serem executadas, desta forma uma pessoa atribuída a um projeto seria atribuída a uma tarefa deste mesmo para que pudesse ser executada.
* Relatório de atividades:
Com todos os dados devidamente cadastrados, seria possível observá-los no foco principal do projeto, o diagrama de Gantt, que consistia que um relatório geral com todas as atividades da empresa.

## Tecnologias
* Java: Buscando atender de forma eficaz a necessidade do cliente, a linguagem Java foi escolhida por possuir o recurso Java Server Pages (JSP) para criação de implementações WEB com a linguagem.
* HTML: Para a criação do Front-end da aplicação foi utilizada a linguagem de marcação HTML.
* CSS: Com o objetivo de atender as demandas de interface, toda a estilização das páginas foi realizada em CSS.
* JavaScript: Visando realizar algumas interações no Front-end foi utilizada a linguagem Javascript
*  MySQL: Para o armazenamento das informações coletadas nos sites, foi utilizado o Sistema de Gerenciamento de Banco de Dados (SGBD), sendo escolhido como indicações de professores no desenvolvimento do projeto.
* Java Server Pages: JavaServer Pages é uma tecnologia que ajuda os desenvolvedores de software a criarem páginas web geradas dinamicamente baseadas em HTML, XML ou outros tipos de documentos.
* Gradle: Para o gerenciamento de dependências do projeto

## Contribuições individuais
Como Master da equipe, realizei um trabalho de facilitador dos possíveis riscos que o projeto poderia apresentar e realizei a organização da priorização das Sprints.<br>
Como facilitador, toda a tarefa repassada para equipe era previamente estudada e implementada no sistema para caso de dúvidas na equipe já fosse possível sanar antes que apresentasse um risco maior.<br>
Para a parte organizacional das Sprints, realizei a seguinte escala de prioridade:
| Atividade  |  Prioridade |
|---|---|
| Criação do diagrama de Gantt  | Alta |
| Criação do carregamento de informações dentro do diagrama  | Alta  |
| Criação de funcionalidade "arrasta e puxa" no diagrama  | Média  |
| Criação de funcionalidade "atribuição de atividade ao projeto" no diagrama  | Média  |
| Criação do cadastro de atividades, tarefas e funcionários  | Média  |
| Criação de funcionalidade para gerenciamento de cadastrados  | Baixa  |

Para uma melhor organização do código, realizei a divisão do mesmo em 4 camadas Factory, Domain, DAO e Servlet. Sendo elas:
* Factory: classe única para conexão com banco de dados
* Domain: múltiplas classes de objetos utilizados no projeto
* DAO: criação de operações dentro do Banco de Dados
* Servlet: para realização de requisições HTTP e regras de negócio.
> Criação do código para conexão com o banco de dados
```java
//Atribuição de elementos fundamentais para conexão com o banco de dados
private static final String USUARIO = "root";
private static final String SENHA = "admin";
private static final String URL = "jdbc:mysql://localhost:3306/pineapple?useTimezone=true&serverTimezone=UTC&useSSL=false";

//Função para conexão com o banco de dados
public  static  Connection  conectar() throws  SQLException {
//Confirguração do driver para conexão
DriverManager.deregisterDriver(new  com.mysql.jdbc.Driver());
//Linha para conexão efetiva com o banco de dados
Connection  conexao = DriverManager.getConnection(URL, USUARIO, SENHA);
return  conexao;
```
Com o código acima é possível realizar a conexão com o banco de dados MySQL.

> Mapeamento encapsulado de objetos como proposta para o desenvolvimento do projeto utilizando POO
```java
public  class  Funcionario {
//Declaração de atributos do objeto Funcionario
private  String  cpf;
private  String  nome;
private  String  email;
//Criação de um método público para pegar a informação de um atributo
public  String  getCpf() {
return  cpf;
}
//Criação de um método privado para inserir a informação em um atributo, garantindo o encapsulamento
private void  setCpf(String  cpf) {
this.cpf = cpf;
}
public  String  getNome() {
return  nome;
}
private void  setNome(String  nome) {
this.nome = nome;
}
public  String  getEmail() {
return  email;
}
private void  setEmail(String  email) {
this.email = email;
}
@Override
public  String  toString() {
String  saida = cpf + " " + nome + " " + email;
return  saida;
}
}
```
O exemplo acima se trata do mapeamento para o objeto Funcionário, foi realizado o mapeamento das demais entidades no mesmo modelo, encapsulando a classe para restringir o acesso.
> Criação dos métodos Create, Read, Update e Delete para Funcionários
```java

//Criação do método de inserção de funcionários
public  class  FuncionarioDAO{
public  void  salvar(Funcionario  f) throws  SQLException{
//Criação da query de INSERT via StringBuilder
StringBuilder  sql = new  StringBuilder();
sql.append("INSERT INTO recurso ");
sql.append("(cpf, nome_recurso, email) ");
//O caracter de "?" vem a ser de substituição para os valores informados pelo usuário
sql.append("VALUES(?,?,?)");

//Chamada na função de conexão
Connection  conexao = ConexaoFactory.conectar();
PreparedStatement  comando = conexao.prepareStatement(sql.toString());
//Realização da substituição dos caracteres
comando.setString(1, f.getCpf());
comando.setString(2, f.getNome());
comando.setString(3, f.getEmail());
//Execução da query montada
comando.executeUpdate();
}

//Método para consulta de funcionários na base de dados
public  Funcionario  consultarCPF(Funcionario  f) throws  SQLException{
StringBuilder  sql = new  StringBuilder();
//Criação da query
sql.append("SELECT cpf, nome_recurso, email ");
sql.append("FROM recurso ");
sql.append("WHERE cpf = ?");

Connection  conexao = ConexaoFactory.conectar();
PreparedStatement  comando = conexao.prepareStatement(sql.toString());
comando.setString(1, f.getCpf());
ResultSet  resultado = comando.executeQuery();
Funcionario  retorno = null;

//Verificação caso fosse encontrado algum resultado na query de pesquisa
if(resultado.next()) {
retorno = new  Funcionario();
retorno.setCpf(resultado.getString("cpf"));
retorno.setNome(resultado.getString("nome_recurso"));
retorno.setEmail(resultado.getString("email"));

}
return  retorno;
}

//Método para atualização de funcionários
public  void  atualizar(Funcionario  f) throws  SQLException{
StringBuilder  sql = new  StringBuilder();
sql.append("UPDATE recurso ");
sql.append("SET nome_recurso = ? , email = ? ");
sql.append("WHERE cpf = ?");

Connection  conexao = ConexaoFactory.conectar();
PreparedStatement  comando = conexao.prepareStatement(sql.toString());
comando.setString(1, f.getNome());
comando.setString(2, f.getEmail());
comando.setString(3, f.getCpf());
comando.executeUpdate();
}

//Método para exclusão de funcionários
public  void  excluir(Funcionario  f) throws  SQLException{
StringBuilder  sql = new  StringBuilder();
sql.append("DELETE FROM recurso ");
sql.append("WHERE cpf = ?");
Connection  conexao = ConexaoFactory.conectar();
PreparedStatement  comando = conexao.prepareStatement(sql.toString());
comando.setString(1, f.getCpf());
comando.executeUpdate();
}

//Método para retornar uma consulta por vários funcionários
public  ArrayList<Funcionario> listar() throws  SQLException{
StringBuilder  sql = new  StringBuilder();
sql.append("SELECT cpf, nome_recurso, email ");
sql.append("FROM recurso ");

Connection  conexao = ConexaoFactory.conectar();
PreparedStatement  comando = conexao.prepareStatement(sql.toString());
ResultSet  resultado = comando.executeQuery();
ArrayList<Funcionario> lista = new  ArrayList<Funcionario>();

//Bloco de repetição para montar uma lista com todos os funcionários
while(resultado.next()) {
Funcionario  f = new  Funcionario();
f.setCpf(resultado.getString("cpf"));
f.setNome(resultado.getString("nome_recurso"));
f.setEmail(resultado.getString("email"));
lista.add(f);
}
return  lista;
}
}
```
O código acima de refere a todos os métodos necessários para tratativa de Funcionários dentro do escopo do projeto, sendo possível realizar consultas, cadastros, modificações e exclusões.
> Método utilizado para requisições HTTP para cadastro de funcionários
```java
protected  void  doPost(HttpServletRequest  request, HttpServletResponse  response) throws  ServletException, IOException {
//Busca de parâmetros dentro do formulário HTTP
String  nome = request.getParameter("nome");
String  email = request.getParameter("email");
String  cpf = request.getParameter("cpf");
//Instando variável para funcionário
Funcionario  registro = new  Funcionario();
//Preenchimento de atributos
registro.setCpf(cpf);
registro.setEmail(email);
registro.setNome(nome);

//Bloco Try/Catch para verificar se foi possível salvar o registro
try {
fdao.salvar(registro);
//caso fosse possível inserir o funcionário, o projeto redirecionaria o usuário para o endereço informado
response.sendRedirect("index.jsp#t2");
}catch(SQLException  ex) {
//Para um melhor mapeamento de erros foi utilizados StackTraces no projeto que permitiam verificar de forma detalhada onde estaria o erro apresentado
ex.printStackTrace();
ex.getMessage();
}
}
```
Acima é representado o que definiu como modelo todas as chamadas de todos os objetos dentro do projeto, através de requisições HTTP foi possível chamar todo o mapeamento de funções da camada DAO.
>Código HTML de layout da página
```html
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
pageEncoding="ISO-8859-1"%>
<!DOCTYPE  html>
<html>
<head>
<meta  charset="ISO-8859-1">
<link  rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link  rel="stylesheet"  type="text/css"  href="assets/css/styles.css">
<link  rel="stylesheet"  type="text/css"  href="jsgantt.css"/>
<script  src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<link  rel="stylesheet"  href="https://www.w3schools.com/w3css/4/w3.css">
<script  language="javascript"  src="jsgantt.js"></script>
<script  language="javascript"  src="graphics.js"></script>
<script  src="codebase/dhtmlxgantt.js?v=7.0.4"></script>
<link  rel="stylesheet"  href="codebase/dhtmlxgantt.css?v=7.0.4">
<title>Pineapple</title>
</head>
<body>
<div  class="ct"  id="t1">
<div  class="ct"  id="t2">
<div  class="ct"  id="t3">
<div  class="ct"  id="t4">
<div  class="ct"  id="t5">
<ul  id="menu">
<a  href="#t1"><li  class="icon fa fa-bolt"  id="uno"></li></a>
<a  href="#t2"><li  class="icon fa fa-keyboard-o"  id="dos"></li></a>
<a  href="#t3"><li  class="icon fa fa-rocket"  id="tres"></li></a>
<a  href="#t4"><li  class="icon fa fa-dribbble"  id="cuatro"></li></a>
</ul>
<div  class="page"  id="p1">
<section  class="icon fa fa-bolt"><span  class="title">Relat�rio</span></section>
<%@ include file="includes/grafico.jsp" %>
</div>
<div  class="page"  id="p2">
<%@ include file="includes/cadastroFuncionario.jsp" %>
</div>
<div  class="page"  id="p3">
<%@ include file="includes/cadastroProjeto.jsp" %>
</div>
<div  class="page"  id="p4">
<%@ include file="includes/cadastroTarefas.jsp" %>
</div>
</div>
</div>
</div>
</div>
</div>
<script  src="assets/js/actions.js"></script>
</body>
</html>
```
Com objetivo de atender o propósito de ser flexível e intuitivo, foi pensado em desenvolver uma interface em "camadas" onde toda a aplicação seria feita em uma página única com a camada principal do gráfico e subcamadas de cadastro, alterações e exclusões de objetos. Este método permitiu um desenvolvimento mais ágil da ferramenta e a concepção de uma página com layout agradável e intuitivo conforme as imagens abaixo.
>Camada principal para o gráfico de Gantt
<div align="center">
    <img src="./Pinneaple/graficopin.png" width="720" />
</div>

>Camada para cadastro de funcionários
<div align="center">
    <img src="./Pinneaple/cadastropin.png" width="720" />
</div>

>Camada para cadastro de tarefas
<div align="center">
    <img src="./Pinneaple/tarefaspin.png" width="720" />
</div>

## Aprendizados efetivos
* Aprendizado de Programação Orientada a Objetos (POO);<br>
Para lidar com uma demanda mais elevada de funções que o projeto propôs, foi fundamental a utilização de POO, através de diversas experiências antes de adotar esta prática, foi possível entender a importância organizacional do código para que todos os integrantes possam compreender da forma correta;
* Aprendizado de desenvolvimento de aplicações com Java Server Pages (JSP);<br>
O primeiro contato com frameworks durante o curso, a utilização deste serviu de exemplo para a procura de outros que viriam a trazer uma maior agilidade para o desenvolvimento de outros projetos.
* Aprendizado de trabalho com requisições HTTP;<br>
O projeto foi feito para ser inserido em um servidor, por sua vez a grande maioria dos eventos que existiam eram realizados via requisição HTTP, nesta etapa foi aprendido referente aos tipos de requisições GET, POST e PUT.
* Aprendizado referente as 10 Heurísticas de Nielsen;<br>
Como o cliente solicitou e sugerido pelo docente da Fatec, foi estudado sobre as 10 Heurísticas de Nielsen que citam tópicos importantes para elaboração de uma interface mais intuitiva e agradável para todos os usuários.
* Aprendizado de administração de projetos como Master;<br>
Neste projeto assumi o papel de Master da equipe, com isso foi possível entender a importância de uma boa gestão nas atividades do grupo e como lidar com possíveis riscos durante o desenvolvimento do projeto.
* Noções de desenvolvimento com JavaScript;<br>
Para os eventos do Diagrama de Gantt foi fundamental o entendimento da linguagem JavaScript e suas funções para a dinamicidade da manipulação dos dados dentro do projeto.
* Aprendizado de criação da documentação do projeto;<br>
Com o auxílio do docente da Fatec, foi possível desenvolver uma documentação mais elaborada e profissional referente ao projeto, apontando o que realmente é necessário informar na mesma e como desenvolver a mesma da melhor forma.
* Utilização ferramenta Gradle<br>
Visando uma melhor eficaz na configuração do projeto no computador de cada integrante, foi utilizado a ferramenta Gradle que se mostrou um ótimo gerenciador de dependências do projeto, trazendo agilidade e facilidade na utilização.